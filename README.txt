=CiviCRM Field Storage Module=

==Authors==

Matt Chapman <Matt@NinjitsuWeb.com>


==Rationale==

Allows Drupal applications to use & store data in CiviCRM, using Drupal APIs, without having to use any of CiviCRM's underlying libraries.


==Description==

**This module is a work in progress. Many capabilities are not yet implemented.**

This module uses the CiviCRM API and the Drupal Field Type and Field Storages Engines APIs to expose CiviCRM data to Drupal, without any
duplication of data or synchronization necessary. Changes to CiviCRM data are immediately reflected in Drupal entities that use CiviCRM fields,
and changes to CiviCRM fields via Drupal Entities are automatically reflected in CiviCRM.

This will allow Drupal developers to display and manipulate CiviCRM data using the full suite of Drupal Entity and Field modules.


==Proof-of-Concept demo==

1. Install CiviCRM and Webform CiviCRM module
2. Make sure your CiviCRM contact record conatins First Name, Last Name, and a Primary Email address
3. Enable civicrm_field_example
4. Visit 'node/add/civicrm-bio'
5. Note that your CiviCRM Contact data is pre-populated in the First Name, Last Name, and Email fields.
6. Add a title (required).
7. Save the node.
8. Note that CiviCRM data is displayed on the node view page.
9. Edit your Contatc record in CiviCRM, changing one of the fields.
10. Refresh the node view; notice that the CiviCRM fields are automatically up-to-date.
11. Edit the node, and change one of the CiviCRM fields. Save.
12. In your CiviCRM Contact record, note that the change has been recorded.
13. Visit admin/structure/types/manage/civicrm_bio/display and admin/structure/types/manage/civicrm_bio/fields.
    Note that CiviCRM fields can be manipulated like other Drupal fields.


==To Do==
- See @todo comments in civicrm_field.module

- Test with various Drupal modules
-- Auto Node Titles
-- Display Suite
-- etc